import socketio
from requests import post
from requests import get
sio = socketio.Client()
sio.connect('https://sockets.streamlabs.com?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbiI6IkYzQUZBN0EwOUJDNDczNjFBNzdDIiwicmVhZF9vbmx5Ijp0cnVlLCJwcmV2ZW50X21hc3RlciI6dHJ1ZSwidHdpdGNoX2lkIjoiNDMwNDYyNzkiLCJ5b3V0dWJlX2lkIjoiVUNHNVRJZ1RBZ2x2TGkzZ0hKeWJUX21nIiwibWl4ZXJfaWQiOiIyODAzMTkwMyJ9.UqmVbjexbdQs66ALSDWFgeUg8HnzbIMAC5n1kTPxwho')
cofee = 1
box = "http://writeescape.myddns.me:"

link = box+"8123/api/"
coffee_machine = box+"2080/api/192.168.1.166"
coffee_cups = get(coffee_machine+"/status")
cups = coffee_cups.json()
coffee = cups["status"]
headers = {
    "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJjMmNmYjg3YTZkYmU0MjljOTRiNWY0N2RlNzg2YzEyMiIsImlhdCI6MTYwNzE4NzE5NywiZXhwIjoxOTIyNTQ3MTk3fQ.ZsVm54ecLeCFcrDc5y9o9k47oC69uVwjngPgR4hDrc0",
    "content-type": "application/json"
}
default_light = {"entity_id": "light.hue_color_lamp_1", "brightness_pct": 50, "rgb_color": [255, 255, 255]}


@sio.event
def connect():
    default_light["rgb_color"] = [100, 65, 165]
    default_light["brightness_pct"] = 75
    post(link+"services/light/turn_on", headers=headers, json=default_light)


@sio.event
def connect_error():
    print("The connection failed!")

@sio.event
def disconnect():
    print("I'm disconnected!")
    post(link+"services/light/turn_on", headers=headers, json=default_light)

@sio.on('event')
def message(data):
    print(data)
    datatype = data.get('type')

    datamsg = data.get('message')


    don_amt = 0
    msg = ""
    name = ""


    if datatype == "donation" or datatype == "bits":
        datames = datamsg[0]
        # get first element in array
        amt = datames.get("formatted_amount")

        don_amt = int(float(amt[1:]))
        msg = datames.get("message")
        #if "coffee" in msg:
        howmuchcoffee = don_amt / 2
        if howmuchcoffee < 1:
            howmuchcoffee = 1
        elif howmuchcoffee > 12:
            howmuchcoffee = 1
        howmuchcoffee = str(round(howmuchcoffee))
        cup = get(coffee_machine + "/cups/"+howmuchcoffee)
        if "coffee" in msg:
          out = get(coffee_machine + "/start")
          print(out)
        name = datames.get("name")

    elif datatype == "subscription" or datatype == "resub" or datatype == "follow":
        datames = datamsg[0]
        name = datames.get("name")
        don_amt = datames.get("months")
        msg = datames.get("message")
        default_light["entity_id"] = "light.hue_white_lamp_1"
        default_light["flash"] = "short"
        post(link + "services/light/turn_on", headers=headers, json=default_light)

    elif datatype == "raid":
        datames = datamsg[0]
        name = datames.get("name")
        don_amt = datames.get("raiders")

        default_light["entity_id"] = "light.hue_color_lamp_2"
        default_light["brightness_pct"] = 100
        default_light["flash"] = "short"
        default_light["rgb_color"] = [255, 100, 100]
        post(link + "services/light/turn_on", headers=headers, json=default_light)

    elif datatype == "host":
        datames = datamsg[0]
        name = datames.get("name")
        don_amt = datames.get("viewers")

        default_light["entity_id"] = "light.hue_color_lamp_2"
        default_light["brightness_pct"] = 100
        default_light["flash"] = "short"
        default_light["rgb_color"] = [255, 100, 100]
        post(link + "services/light/turn_on", headers=headers, json=default_light)

    elif datatype == "pledge":
        datames = datamsg[0]
        don_amt = datames.get("amount")
        name = datames.get("name")
    else:
        name = ""
        msg = ""

    don_amt = str(don_amt)
    print("Type:"+datatype+" Name:"+name+" Amount:"+don_amt + " Message:"+msg)